package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.data.AppointmentsProvider
import com.dlgdev.doghandling.model.data.DatabaseProvider
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test

internal class CancelAppointmentTest {

    val appointment = Appointment(0, 0, 0L, 0L, "", "", "", "", false)
    val timeEnd = 1234L
    val behavior = "good boy"
    val cost = "tropecientos leuros"
    val treatment = "mimos"
    val notes = "el perro era una gata"

    var prov: AppointmentsProvider = mock<DatabaseProvider>()
    var useCase = CancelAppointment(prov)

    @Test
    fun cancelAppointment() {
        useCase.cancelAppointment(appointment, timeEnd, cost, behavior, notes)
        verify(prov).finishAppointment(eq(appointment), eq(timeEnd), eq(cost), eq(behavior), eq(notes), eq(true))
    }
}