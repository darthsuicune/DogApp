package com.dlgdev.doghandling.model.data

import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import com.nhaarman.mockitokotlin2.argThat
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class DatabaseProviderTest {

    var dog = Dog("name", "ownerName", "phone", "comment")
    var location = Location("locname", "locaddress", "0123545163", "notes")
    var appointment = Appointment(0, 0, 0, 0, "", "", "", "")

    lateinit var prov: DatabaseProvider

    lateinit var dogDao: DogDao
    lateinit var appointmentDao: AppointmentDao
    lateinit var locationDao: LocationDao

    @BeforeEach fun before() {
        dogDao = mock()
        appointmentDao = mock()
        locationDao = mock()
        prov = DatabaseProvider(dogDao, appointmentDao, locationDao)
    }

    @Test fun testCreateDog() {
        createDog()
        verify(dogDao, times(1)).insert(argThat { dogName == dog.dogName })
    }

    fun createDog() {
        dog = prov.createDog(dog.dogName, dog.ownersName, dog.dogContactPhone, dog.dogNotes, location)
    }

    @Test fun getDogs() {
        createDog()
        prov.getDogs()
        verify(dogDao, times(1)).liveDogs()
    }

    @Test fun editDog() {
        val newName = "new name"
        val newOwner = "new owner"
        val phone = "new phone"
        val comments = "some new stuff"
        prov.editDog(dog, newName, newOwner, phone, comments)
        assertEquals(newName, dog.dogName)
        assertEquals(newOwner, dog.ownersName)
        assertEquals(phone, dog.dogContactPhone)
        assertEquals(comments, dog.dogNotes)
        verify(dogDao, times(1)).updateDog(argThat { dogName == newName })
    }

    @Test fun testCreateAppointment() {
        createAppointment()
        verify(appointmentDao).insert(argThat { timeStart == 0L && treatment == "" && notes == "" })
    }

    fun createAppointment() {
        createDog()
        createLocation()

        appointment = prov.createAppointment(dog, location, 0L, "", "", "")
    }

    @Test fun getAppointments() {
        createAppointment()
        prov.getAppointmentsWithDogsAndLocations()
        verify(appointmentDao).appointmentsWithDogAndLocation()
    }

    @Test fun finishAppointment() {
        val newTimeEnd = 1234L
        val newBehavior = "good boy"
        val newCost = "1 euro plis"
        val newNotes = "el perro era un gato"
        val app = prov.finishAppointment(appointment, newTimeEnd, newCost, newBehavior, newNotes)
        assertEquals(newTimeEnd, app.timeEnd)
        assertEquals(newBehavior, app.behavior)
        assertEquals(newCost, app.cost)
        assertEquals(newNotes, app.notes)
        verify(appointmentDao).update(appointment)
    }

    @Test fun editAppointment() {
        val newTimeStart = 123L
        val newTimeEnd = 123L
        val newTreatment = "treat"
        val newNotes = "notes"
        val newCost = "cost"
        val newBehavior = "behavior"
        val canceled = true

        dog.dogId = 123
        location.locationId = 123

        val app = prov.editAppointment(appointment, dog, location, newTimeStart, newTimeEnd,
                newTreatment, newNotes, newCost, newBehavior, canceled)
        assertEquals(newTimeStart, app.timeStart)
        assertEquals(newTimeEnd, app.timeEnd)
        assertEquals(newTreatment, app.treatment)
        assertEquals(newNotes, app.notes)
        assertEquals(newCost, app.cost)
        assertEquals(newBehavior, app.behavior)
        assertEquals(123, app.dogId)
        assertEquals(123, app.locationId)
        assertTrue(app.canceled)
        verify(appointmentDao).update(appointment)
    }

    @Test fun testCreateLocation() {
        createLocation()
        verify(locationDao).insert(argThat { locationName == location.locationName && locationAddress == location.locationAddress })
    }

    fun createLocation() {
        location = prov.createLocation(location.locationName, location.locationAddress, location.locationContactPhone, location.locationNotes)
    }
}