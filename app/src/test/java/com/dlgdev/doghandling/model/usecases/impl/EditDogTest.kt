package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.data.DatabaseProvider
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test

internal class EditDogTest {

    val dog = Dog("", "", "", "")
    val name = "New name"
    val ownersName = "New owner"
    val phone = "12359187"
    val comment = "some stuff"

    val prov: DatabaseProvider = mock()
    val useCase: EditDog = EditDog(prov)

    @Test
    fun editAppointment() {
        useCase.editDog(dog, name, ownersName, phone, comment)
        verify(prov).editDog(dog, name, ownersName, phone, comment)
    }
}