package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.DatabaseProvider
import com.dlgdev.doghandling.model.data.DogsProvider
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test

internal class CreateDogTest {

    val name = "Pluto"
    val ownersName = "Yo"
    val phone = "01234151235"
    val comment: String = "comment"

    val prov: DogsProvider = mock<DatabaseProvider>()
    val useCase: CreateDog = CreateDog(prov)

    val location = Location("", "", "", "")

    @Test
    fun createNewDog() {
        useCase.createNewDog(name, ownersName, phone, comment, location)
        verify(prov).createDog(name, ownersName, phone, comment, location)
    }
}