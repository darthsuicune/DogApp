package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.data.DatabaseProvider
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test

internal class FinishAppointmentTest {

    val appointment = Appointment(0, 0, 0, 0L, "", "", "", "")
    val timeEnd = 1234L
    val behavior = "good boy"
    val cost = "tropecientos leuros"
    val notes = "el perro era una gata"

    val prov: DatabaseProvider = mock()
    val useCase: FinishAppointment = FinishAppointment(prov)

    @Test fun finishAppointment() {
        useCase.finishAppointment(appointment, timeEnd, cost, behavior, notes)
        verify(prov).finishAppointment(appointment, timeEnd, cost, behavior, notes, false)
    }
}