package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.DatabaseProvider
import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test

class CreateAppointmentTest {
    val dogName = "dog name"
    val owner = "owner name"
    val phone = "0123456789"
    val dogComments = "comment"
    val timeStart = 0L
    val treatment = "treatment"
    val notes = "notes"
    val cost = "cost"

    val  dog: Dog = Dog(dogName, owner, phone, dogComments, 0)
    val location = Location("name", "location", "12152", "asdf")

    val prov = mock<DatabaseProvider> {
        on {createDog(any(), any(), any(), any(), any())}.doReturn(dog)
    }

    val useCase = CreateAppointment(prov)

    @Test
    fun createWithExistingDog() {
        useCase.create(dog, location, timeStart, treatment, notes, cost)
        verify(prov).createAppointment(dog, location, timeStart, treatment, notes, cost)
    }
}