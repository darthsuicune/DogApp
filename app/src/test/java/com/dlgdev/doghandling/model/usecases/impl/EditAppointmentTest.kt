package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.DatabaseProvider
import com.nhaarman.mockitokotlin2.argForWhich
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test

internal class EditAppointmentTest {

    val appointment = Appointment(0, 0, 0, 0L, "", "", "", "")
    val timeStart = 123L
    val timeEnd = 1234L
    val behavior = "good boy"
    val cost = "tropecientos leuros"
    val treatment = "mimos"
    val notes = "el perro era una gata"
    val location = Location("name", "en mi casa", "614964", "asdf")
    val dog = Dog("name", "owner", "phone", "comment")

    val prov: DatabaseProvider = mock()
    var useCase: EditAppointment = EditAppointment(prov, prov)

    @Test
    fun editAppointment() {
        useCase.editAppointment(appointment, dog, location, timeStart, timeEnd, treatment, notes,
                cost, behavior)
        verify(prov).editAppointment(
                argForWhich { timeStart == this.timeStart && timeEnd == this.timeEnd && treatment == this.treatment && notes == this.notes && behavior == this.behavior && cost == this.cost },
                eq(dog), eq(location), eq(timeStart), eq(timeEnd), eq(treatment), eq(notes),
                eq(cost), eq(behavior), eq(false))
    }
}