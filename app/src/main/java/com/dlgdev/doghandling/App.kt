package com.dlgdev.doghandling

import com.dlgdev.doghandling.dagger.DaggerApplicationComponent
import com.dlgdev.doghandling.dagger.DbModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Inject

class App: DaggerApplication() {
    @Inject lateinit var injector : DispatchingAndroidInjector<App>

    override fun onCreate() {
        DaggerApplicationComponent.builder()
                .application(this)
                .build()
                .inject(this)
        super.onCreate()
    }

    override fun applicationInjector(): AndroidInjector<out App> = injector
}