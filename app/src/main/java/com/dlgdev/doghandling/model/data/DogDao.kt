package com.dlgdev.doghandling.model.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.DogWithLocation

@Dao
interface DogDao {
    @Query("SELECT * FROM dogs")
    fun liveDogs(): LiveData<List<Dog>>

    @Query("SELECT * FROM dogs")
    fun allDogs(): List<Dog>

    @Query("SELECT * FROM dogs WHERE dogId=:id LIMIT 1")
    fun dogWithId(id: Long): LiveData<Dog>

    @Query("SELECT * FROM dogs WHERE dogName LIKE :name")
    fun dogsFromName(name: String): LiveData<List<Dog>>

    @Transaction
    @Query("SELECT * FROM dogs")
    fun dogsWithLocation(): LiveData<List<DogWithLocation>>

    @Insert fun insert(dog: Dog): Long
    @Update fun updateDog(dog: Dog)
    @Delete fun delete(dog: Dog)

}