package com.dlgdev.doghandling.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "appointments",
        foreignKeys = ([
                ForeignKey(entity = Dog::class,
                        parentColumns = ["dogId"],
                        childColumns = ["dogId"]),
                ForeignKey(entity = Location::class,
                        parentColumns = ["locationId"],
                        childColumns = ["locationId"])
        ]))
data class Appointment(var dogId: Long,
                       var locationId: Long,
                       var timeStart: Long,
                       var timeEnd: Long = 0L,
                       var treatment: String,
                       var notes: String,
                       var cost: String,
                       var behavior: String = "",
                       var canceled: Boolean = false) {
    @PrimaryKey(autoGenerate = true) var appointmentId: Long = 0

    fun displayStartDate() = SimpleDateFormat.getInstance().format(Date(timeStart))
    fun displayEndDate() = SimpleDateFormat.getInstance().format(Date(timeEnd))
}