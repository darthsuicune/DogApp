package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.DogsProvider
import com.dlgdev.doghandling.model.usecases.CreateDogUseCase
import javax.inject.Inject

class CreateDog @Inject constructor(val provider: DogsProvider) :
        CreateDogUseCase {
    override fun createNewDog(name: String, ownersName: String, contactPhone: String,
                              notes: String, location: Location) {
        provider.createDog(name, ownersName, contactPhone, notes, location)
    }
}