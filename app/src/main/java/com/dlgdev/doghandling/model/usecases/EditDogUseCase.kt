package com.dlgdev.doghandling.model.usecases

import com.dlgdev.doghandling.model.Dog

interface EditDogUseCase {
    fun editDog(dog: Dog, newName: String, newOwner: String, newPhone: String, newComment: String)
}