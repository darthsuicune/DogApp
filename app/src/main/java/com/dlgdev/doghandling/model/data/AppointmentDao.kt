package com.dlgdev.doghandling.model.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.AppointmentWithDogAtLocation

@Dao
interface AppointmentDao {
    @Query("SELECT * FROM appointments")
    fun allAppointments(): LiveData<List<Appointment>>

    @Transaction
    @Query("SELECT * FROM appointments WHERE appointmentId=:id LIMIT 1")
    fun appointmentWithId(id: Long): LiveData<AppointmentWithDogAtLocation>

    @Query("SELECT * FROM appointments WHERE locationId=:locationId")
    fun appointmentsFromLocation(locationId: Long): LiveData<List<Appointment>>

    @Query("SELECT * FROM appointments WHERE dogId=:dogId")
    fun appointmentsWithDog(dogId: Long): LiveData<List<Appointment>>

    //TODO: Ensure its the last one
    @Query("SELECT * FROM appointments WHERE dogId=:dogId LIMIT 1")
    fun lastAppointmentsWithDog(dogId: Long): LiveData<List<Appointment>>

    @Transaction
    @Query("SELECT * FROM appointments")
    fun appointmentsWithDogAndLocation(): LiveData<List<AppointmentWithDogAtLocation>>


    @Insert fun insert(appointment: Appointment): Long
    @Update fun update(appointment: Appointment)
    @Delete fun delete(appointment: Appointment)
}