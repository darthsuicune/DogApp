package com.dlgdev.doghandling.model

import androidx.room.Embedded
import androidx.room.Relation

data class AppointmentWithDogAtLocation(
        @Relation(parentColumn = "dogId",
                entityColumn = "dogId")
        val dog: Dog,
        @Relation(parentColumn = "locationId",
                entityColumn = "locationId")
        val location: Location,
        @Embedded val appointment: Appointment) {
    fun appointmentId(): Long = appointment.appointmentId

}