package com.dlgdev.doghandling.model.usecases

import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location

interface EditAppointmentUseCase {
    fun editAppointment(appointment: Appointment, dog: Dog, location: Location, timeStart: Long,
                        timeEnd: Long, treatment: String, notes: String, cost: String,
                        behavior: String, canceled: Boolean = false)

    fun editAppointmentWithNewDog(appointment: Appointment, location: Location, timeStart: Long,
                                  timeEnd: Long, treatment: String, notes: String,
                                  cost: String, behavior: String, canceled: Boolean = false,
                                  name: String,ownersName: String, phone: String, comments: String)
}