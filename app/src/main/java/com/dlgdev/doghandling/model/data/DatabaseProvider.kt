package com.dlgdev.doghandling.model.data

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.dlgdev.doghandling.model.*
import javax.inject.Inject

open class DatabaseProvider @Inject constructor(val dogDao: DogDao,
                                                val appointmentDao: AppointmentDao,
                                                val locationDao: LocationDao) :
        AppointmentsProvider, DogsProvider, LocationsProvider {
    override fun getDogForId(id: Long): LiveData<Dog> = dogDao.dogWithId(id)

    override fun getLocationForId(id: Long): LiveData<Location> = locationDao.locationWithId(id)

    override fun findAppointmentById(id: Long): LiveData<AppointmentWithDogAtLocation> =
            appointmentDao.appointmentWithId(id)

    override fun getDogs(): LiveData<List<Dog>> {
        return dogDao.liveDogs()
    }

    @WorkerThread
    override fun createDog(name: String, ownersName: String, contactPhone: String,
                           comments: String, location: Location): Dog {
        val goodboy = Dog(name, ownersName, contactPhone, comments, location.locationId)
        goodboy.dogId = dogDao.insert(goodboy)
        return goodboy
    }

    @WorkerThread
    override fun editDog(dog: Dog, newName: String, newOwner: String, newPhone: String,
                         newComment: String) {
        dog.dogNotes = newComment
        dog.dogContactPhone = newPhone
        dog.ownersName = newOwner
        dog.dogName = newName
        dogDao.updateDog(dog)
    }

    override fun getDogsWithLocation(): LiveData<List<DogWithLocation>> {
        return dogDao.dogsWithLocation()
    }

    @WorkerThread
    override fun createAppointment(dog: Dog, location: Location, timeStart: Long, treatment: String,
                                   notes: String, cost: String): Appointment {

        val appointment = Appointment(dog.dogId, location.locationId, timeStart, 0L, treatment,
                notes, cost)
        appointment.appointmentId = appointmentDao.insert(appointment)
        return appointment
    }

    @WorkerThread
    override fun editAppointment(appointment: Appointment, newDog: Dog, newLocation: Location,
                                 timeStart: Long, timeEnd: Long, treatment: String, notes: String,
                                 cost: String, behavior: String, canceled: Boolean): Appointment {
        appointment.dogId = newDog.dogId
        appointment.locationId = newLocation.locationId
        appointment.timeStart = timeStart
        appointment.timeEnd = timeEnd
        appointment.treatment = treatment
        appointment.notes = notes
        appointment.cost = cost
        appointment.behavior = behavior
        appointment.canceled = canceled
        appointmentDao.update(appointment)
        return appointment
    }

    @WorkerThread
    override fun finishAppointment(appointment: Appointment, timeEnd: Long, cost: String,
                                   behavior: String, notes: String,
                                   canceled: Boolean): Appointment {
        appointment.timeEnd = timeEnd
        appointment.behavior = behavior
        appointment.cost = cost
        appointment.notes = notes
        appointment.canceled = canceled
        appointmentDao.update(appointment)
        return appointment
    }

    override fun getAppointmentsWithDogsAndLocations(): LiveData<List<AppointmentWithDogAtLocation>> {
        return appointmentDao.appointmentsWithDogAndLocation()
    }

    override fun getLocations(): LiveData<List<Location>> {
        return locationDao.liveLocations()
    }

    @WorkerThread
    override fun createLocation(name: String, address: String, contactPhone: String,
                                notes: String): Location {
        val location = Location(name, address, contactPhone, notes)
        location.locationId = locationDao.insert(location)
        return location
    }

    @WorkerThread
    override fun editLocation(location: Location, name: String, address: String,
                              contactPhone: String, notes: String) {
        location.locationName = name
        location.locationAddress = address
        location.locationContactPhone = contactPhone
        location.locationNotes = notes
        locationDao.update(location)
    }
}

