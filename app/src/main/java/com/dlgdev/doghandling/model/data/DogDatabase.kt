package com.dlgdev.doghandling.model.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location

@Database(entities = [(Dog::class), (Appointment::class), (Location::class)], version = 1,
        exportSchema = false)
abstract class DogDatabase: RoomDatabase() {
    abstract fun dogDao(): DogDao
    abstract fun appointmentDao(): AppointmentDao
    abstract fun locationDao(): LocationDao

    companion object: Migration(1, 2) {
        override fun migrate(db: SupportSQLiteDatabase) {
        }
    }
}
