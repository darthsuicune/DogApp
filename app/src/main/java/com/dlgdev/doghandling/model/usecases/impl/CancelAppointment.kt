package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.data.AppointmentsProvider
import com.dlgdev.doghandling.model.usecases.CancelAppointmentUseCase
import javax.inject.Inject

class CancelAppointment @Inject constructor(val provider: AppointmentsProvider):
        CancelAppointmentUseCase {
    override fun cancelAppointment(appointment: Appointment,
                                   timeEnd: Long,
                                   cost: String,
                                   behavior: String,
                                   notes: String) {
        provider.finishAppointment(appointment, timeEnd, cost, behavior, notes, true)
    }
}