package com.dlgdev.doghandling.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "dogs",
        foreignKeys = ([ForeignKey(entity = Location::class, parentColumns = ["locationId"],
                childColumns = ["dogDefaultLocationId"])]))
data class Dog(var dogName: String,
               var ownersName: String,
               var dogContactPhone: String,
               var dogNotes: String,
               var dogDefaultLocationId: Long = -1) {
    @PrimaryKey(autoGenerate = true) var dogId: Long = 0

    override fun toString(): String {
        return "$dogName - $ownersName"
    }
}
