package com.dlgdev.doghandling.model.data

import androidx.lifecycle.LiveData
import com.dlgdev.doghandling.model.Location

interface LocationsProvider {
    fun getLocationForId(id: Long): LiveData<Location>
    fun getLocations(): LiveData<List<Location>>
    fun createLocation(name: String, address: String, contactPhone: String, notes: String): Location
    fun editLocation(location: Location, name: String, address: String, contactPhone: String,
                     notes: String)
}