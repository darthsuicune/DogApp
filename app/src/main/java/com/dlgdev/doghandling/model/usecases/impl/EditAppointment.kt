package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.AppointmentsProvider
import com.dlgdev.doghandling.model.data.DogsProvider
import com.dlgdev.doghandling.model.usecases.EditAppointmentUseCase
import javax.inject.Inject

class EditAppointment @Inject constructor(val provider: AppointmentsProvider,
                                          val dogsProvider: DogsProvider) :
        EditAppointmentUseCase {
    override fun editAppointment(appointment: Appointment, newDog: Dog, location: Location,
                                 timeStart: Long, timeEnd: Long, treatment: String, notes: String,
                                 cost: String, behavior: String, canceled: Boolean) {
        provider.editAppointment(appointment, newDog, location, timeStart, timeEnd, treatment,
                notes, cost, behavior, canceled)
    }

    override fun editAppointmentWithNewDog(appointment: Appointment, location: Location,
                                           timeStart: Long, timeEnd: Long, treatment: String,
                                           notes: String, cost: String, behavior: String,
                                           canceled: Boolean, name: String, ownersName: String,
                                           phone: String, comments: String) {
        val newDog = dogsProvider.createDog(name, ownersName, phone, comments, location)
        provider.editAppointment(appointment, newDog, location, timeStart, timeEnd, treatment,
                notes, cost, behavior, canceled)
    }
}