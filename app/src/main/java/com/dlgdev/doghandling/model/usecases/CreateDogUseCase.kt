package com.dlgdev.doghandling.model.usecases

import com.dlgdev.doghandling.model.Location

interface CreateDogUseCase {
    fun createNewDog(name: String, ownersName: String, contactPhone: String,
                     notes: String, location: Location)
}