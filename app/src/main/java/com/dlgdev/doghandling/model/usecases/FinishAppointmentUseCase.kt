package com.dlgdev.doghandling.model.usecases

import com.dlgdev.doghandling.model.Appointment

interface FinishAppointmentUseCase {
    fun finishAppointment(appointment: Appointment,
                          timeEnd: Long,
                          cost: String,
                          behavior: String,
                          notes: String)
}