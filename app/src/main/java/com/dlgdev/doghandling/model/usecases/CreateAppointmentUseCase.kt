package com.dlgdev.doghandling.model.usecases

import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location

interface CreateAppointmentUseCase {
    fun create(dog: Dog, location: Location, timeStart: Long, treatment: String,
                              notes: String, cost: String)
}