package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.data.AppointmentsProvider
import com.dlgdev.doghandling.model.usecases.FinishAppointmentUseCase
import javax.inject.Inject

class FinishAppointment @Inject constructor(val provider: AppointmentsProvider) :
        FinishAppointmentUseCase {
    override fun finishAppointment(appointment: Appointment,
                                   timeEnd: Long, cost: String,
                                   behavior: String,
                                   notes: String) {
        provider.finishAppointment(appointment, timeEnd, cost, behavior, notes)
    }
}