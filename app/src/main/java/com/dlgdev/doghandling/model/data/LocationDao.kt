package com.dlgdev.doghandling.model.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dlgdev.doghandling.model.Location

@Dao
interface LocationDao {
    @Query("SELECT * FROM locations")
    fun liveLocations(): LiveData<List<Location>>

    @Query("SELECT * FROM locations")
    fun allLocations(): List<Location>

    @Query("SELECT * FROM locations WHERE locationId=:id LIMIT 1")
    fun locationWithId(id: Long): LiveData<Location>

    @Query("SELECT * FROM locations WHERE locationName LIKE :name")
    fun locationFromString(name: String): LiveData<List<Location>>

    @Insert fun insert(location: Location): Long
    @Update fun update(location: Location)
    @Delete fun delete(location: Location)
}