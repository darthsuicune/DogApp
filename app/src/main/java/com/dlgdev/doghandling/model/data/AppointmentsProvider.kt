package com.dlgdev.doghandling.model.data

import androidx.lifecycle.LiveData
import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.AppointmentWithDogAtLocation
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location

interface AppointmentsProvider {
    fun getAppointmentsWithDogsAndLocations(): LiveData<List<AppointmentWithDogAtLocation>>
    fun findAppointmentById(id: Long): LiveData<AppointmentWithDogAtLocation>
    fun createAppointment(dog: Dog, location: Location, timeStart: Long, treatment: String,
                          notes: String, cost: String): Appointment

    fun finishAppointment(appointment: Appointment, timeEnd: Long, cost: String, behavior: String,
                          notes: String, canceled: Boolean = false): Appointment

    fun editAppointment(appointment: Appointment, newDog: Dog, newLocation: Location,
                        timeStart: Long,
                        timeEnd: Long, treatment: String, notes: String, cost: String,
                        behavior: String, canceled: Boolean): Appointment
}