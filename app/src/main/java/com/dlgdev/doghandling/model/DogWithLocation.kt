package com.dlgdev.doghandling.model

import androidx.room.Embedded
import androidx.room.Relation

data class DogWithLocation(
        @Relation(parentColumn = "dogDefaultLocationId", entityColumn = "locationId")
        val location: Location,
        @Embedded val dog: Dog
) {
        fun dogId() = dog.dogId
}