package com.dlgdev.doghandling.model.usecases

import com.dlgdev.doghandling.model.Appointment

interface CancelAppointmentUseCase {
    fun cancelAppointment(appointment: Appointment,
                          timeEnd: Long,
                          cost: String,
                          behavior: String,
                          notes: String)
}