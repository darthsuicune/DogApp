package com.dlgdev.doghandling.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "locations")
data class Location(var locationName: String,
                    var locationAddress: String,
                    var locationContactPhone: String,
                    var locationNotes: String) {
    @PrimaryKey(autoGenerate = true) var locationId: Long = 0
}