package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.AppointmentsProvider
import com.dlgdev.doghandling.model.usecases.CreateAppointmentUseCase
import javax.inject.Inject

class CreateAppointment @Inject constructor(val provider: AppointmentsProvider) :
        CreateAppointmentUseCase {
    override fun create(dog: Dog, location: Location, timeStart: Long,
                                       treatment: String, notes: String, cost: String) {
        provider.createAppointment(dog, location, timeStart, treatment, notes, cost)

    }
}