package com.dlgdev.doghandling.model.data

import androidx.lifecycle.LiveData
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.DogWithLocation
import com.dlgdev.doghandling.model.Location

interface DogsProvider {
    fun getDogForId(id: Long): LiveData<Dog>
    fun getDogs(): LiveData<List<Dog>>
    fun createDog(name: String, ownersName: String, contactPhone: String, comments: String, location: Location): Dog
    fun editDog(dog: Dog, newName: String, newOwner: String, newPhone: String, newComment: String)
    fun getDogsWithLocation(): LiveData<List<DogWithLocation>>
}