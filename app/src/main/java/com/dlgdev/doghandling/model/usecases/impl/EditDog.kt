package com.dlgdev.doghandling.model.usecases.impl

import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.data.DogsProvider
import com.dlgdev.doghandling.model.usecases.EditDogUseCase
import javax.inject.Inject

class EditDog @Inject constructor(val provider: DogsProvider) : EditDogUseCase {
    override fun editDog(dog: Dog, newName: String, newOwner: String, newPhone: String,
                         newComment: String) {
        provider.editDog(dog, newName, newOwner, newPhone, newComment)
    }
}