package com.dlgdev.doghandling.ui.locations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import javax.inject.Inject

class AddLocationFragment : DaggerFragmentWithMenu() {

    companion object {
        fun newInstance() = AddLocationFragment()
    }

    @Inject lateinit var factory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_location_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel: AddLocationViewModel by viewModels { factory }
        // TODO: Use the ViewModel
    }

}
