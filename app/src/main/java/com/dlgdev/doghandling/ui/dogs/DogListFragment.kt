package com.dlgdev.doghandling.ui.dogs

import android.os.Bundle
import android.view.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import com.dlgdev.doghandling.ui.dogs.viewmodel.DogListViewModel
import kotlinx.android.synthetic.main.dog_list_fragment.*
import javax.inject.Inject

class DogListFragment : DaggerFragmentWithMenu() {

    companion object {
        fun newInstance() = DogListFragment()
    }

    @Inject lateinit var factory: ViewModelProvider.Factory
    @Inject lateinit var recyclerAdapter: DogAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dog_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener {
            navigateTo(R.id.action_dogListFragment_to_addDogFragment)
        }
        activity?.title = getString(R.string.menu_dogs_title)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.dog_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_appointments -> {
                navigateTo(R.id.action_dogListFragment_to_appointmentListFragment)
                true
            }
            R.id.menu_locations -> {
                navigateTo(R.id.action_dogListFragment_to_locationsListFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel: DogListViewModel by viewModels { factory }
        recyclerAdapter.navController = findNavController()

        dog_list_view.apply {
            adapter = recyclerAdapter
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
        viewModel.dogs.observe(viewLifecycleOwner, Observer {
            recyclerAdapter.updateDogs(it)
        })
    }
}
