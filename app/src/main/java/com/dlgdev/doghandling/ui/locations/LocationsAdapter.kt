package com.dlgdev.doghandling.ui.locations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.Location
import java.util.function.IntConsumer
import javax.inject.Inject

class LocationsAdapter @Inject constructor() : RecyclerView.Adapter<LocationsViewHolder>() {
    var navController: NavController? = null
    var locations: List<Location> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, position: Int) =
            LocationsViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.location_item, parent,
                            false))

    override fun onBindViewHolder(holder: LocationsViewHolder, position: Int) {
        ButterKnife.bind(holder, holder.itemView)
        val location = locations[position]

        holder.location(location, IntConsumer { action ->
                    navController?.navigate(action, Bundle().also {
                        it.putLong(ViewLocationFragment.ARG_LOCATION_ID, location.locationId)
                    })
                })
    }

    override fun getItemCount() = locations.size


    fun updateLocations(locations: List<Location>) {
        this.locations = locations
        this.notifyDataSetChanged()
    }
}