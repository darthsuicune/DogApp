package com.dlgdev.doghandling.ui.locations

import android.os.Bundle
import android.view.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import kotlinx.android.synthetic.main.locations_list_fragment.*
import javax.inject.Inject

class LocationListFragment : DaggerFragmentWithMenu() {

    companion object {
        fun newInstance() = LocationListFragment()
    }

    @Inject lateinit var factory: ViewModelProvider.Factory
    @Inject lateinit var recyclerAdapter: LocationsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.locations_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener {
            navigateTo(R.id.action_locationsListFragment_to_addLocationFragment)
        }
        activity?.title = getString(R.string.menu_locations_title)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.location_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_appointments -> {
                navigateTo(R.id.action_locationsListFragment_to_appointmentListFragment)
                true
            }
            R.id.menu_dogs -> {
                navigateTo(R.id.action_locationsListFragment_to_dogListFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel:LocationListViewModel by viewModels { factory }
        recyclerAdapter.navController = findNavController()

        location_list_view.apply {
            adapter = recyclerAdapter
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        viewModel.locations.observe(viewLifecycleOwner, Observer {
            recyclerAdapter.updateLocations(it)
        })
    }
}
