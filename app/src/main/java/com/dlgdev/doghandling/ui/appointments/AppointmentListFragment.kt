package com.dlgdev.doghandling.ui.appointments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import com.dlgdev.doghandling.ui.appointments.viewmodel.AppointmentListViewModel
import kotlinx.android.synthetic.main.appointment_list_fragment.*
import javax.inject.Inject

class AppointmentListFragment : DaggerFragmentWithMenu() {

    companion object {
        fun newInstance() = AppointmentListFragment()
    }
    @Inject lateinit var recyclerAdapter: AppointmentsRecyclerAdapter
    @Inject lateinit var factory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.appointment_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener {
            navigateTo(R.id.action_appointmentListFragment_to_addAppointmentFragment)
        }
        activity?.title = getString(R.string.menu_appointments_title)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.appointment_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_dogs -> {
                navigateTo(R.id.action_appointmentListFragment_to_dogListFragment)
                true
            }
            R.id.menu_locations -> {
                navigateTo(R.id.action_appointmentListFragment_to_locationsListFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel: AppointmentListViewModel by viewModels { factory }
        recyclerAdapter.navController = findNavController()

        appointment_list_view.apply {
            adapter = recyclerAdapter
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        viewModel.appointments.observe(viewLifecycleOwner, Observer {
            recyclerAdapter.updateAppointments(it)
        })
        //TODO: Just for testing purposes
//        fun createStuff() {
//            val pool = Executors.newFixedThreadPool(5)
//            pool.execute {
//                val rand = Random.nextLong(100)
//                val location = db.createLocation("place$rand", "address$rand", "phone$rand", "notes$rand")
//                val dog = db.createDog("dog$rand", "owner$rand", "phone$rand", "comments$rand", location)
//                val appointment = db.createAppointment(dog, location, rand, "treat$rand", "notes$rand", "$rand")
//                Log.d("Inserting $appointment", "Inserting $rand")
//            }
//        }
    }
}
