package com.dlgdev.doghandling.ui.appointments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.usecases.CancelAppointmentUseCase
import com.dlgdev.doghandling.model.usecases.FinishAppointmentUseCase
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import com.dlgdev.doghandling.ui.appointments.viewmodel.EditAppointmentViewModel
import kotlinx.android.synthetic.main.dog_item.*
import kotlinx.android.synthetic.main.edit_appointment_fragment.*
import kotlinx.android.synthetic.main.location_item.*
import kotlinx.android.synthetic.main.view_appointment_fragment.appointment_behavior
import kotlinx.android.synthetic.main.view_appointment_fragment.appointment_cost
import kotlinx.android.synthetic.main.view_appointment_fragment.appointment_end_time
import kotlinx.android.synthetic.main.view_appointment_fragment.appointment_notes
import kotlinx.android.synthetic.main.view_appointment_fragment.appointment_start_date
import kotlinx.android.synthetic.main.view_appointment_fragment.appointment_treatment
import javax.inject.Inject

class EditAppointmentFragment : DaggerFragmentWithMenu() {

    companion object {
        fun newInstance() = EditAppointmentFragment()
        const val ARG_APPOINTMENT_ID = "appointmentId"
        const val ARG_APPOINTMENT_CANCEL = "cancelAppointment"
    }

    @Inject lateinit var factory: ViewModelProvider.Factory

    @Inject lateinit var finishAppointment: FinishAppointmentUseCase
    @Inject lateinit var cancelAppointment: CancelAppointmentUseCase

    //TODO: Load different layout based on whether canceling is being requested.
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.edit_appointment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cancel = if(arguments != null && arguments!!.containsKey(ARG_APPOINTMENT_CANCEL)) {
            arguments!![ARG_APPOINTMENT_CANCEL] as Boolean
        } else {
            false //TODO: This should use the actual appointment value instead.
        }

        include_dog_info_on_appointment.setOnClickListener {

        }

        include_location_info_on_appointment.setOnClickListener {

        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel: EditAppointmentViewModel by viewModels { factory }

        viewModel.appointmentId = arguments!![ViewAppointmentFragment.ARG_APPOINTMENT_ID] as Long

        viewModel.appointment.observe(viewLifecycleOwner, Observer {
            val appointment = it.appointment
            val dog = it.dog
            val location = it.location
            appointment_start_date.text = appointment.displayStartDate()
            appointment_behavior.text = appointment.behavior
            appointment_cost.text = appointment.cost
            appointment_notes.text = appointment.notes
            appointment_treatment.text = appointment.treatment
            appointment_end_time.text = appointment.displayEndDate()

            dog_item_name.text = dog.dogName
            dog_location.text = dog.ownersName

            location_name.text = location.locationName
            location_address.text = location.locationAddress
        })
    }

}
