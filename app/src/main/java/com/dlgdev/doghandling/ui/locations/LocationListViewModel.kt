package com.dlgdev.doghandling.ui.locations

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.LocationsProvider
import javax.inject.Inject

class LocationListViewModel @Inject constructor(val provider: LocationsProvider): ViewModel() {
    val locations: LiveData<List<Location>> by lazy {
        provider.getLocations()
    }
}
