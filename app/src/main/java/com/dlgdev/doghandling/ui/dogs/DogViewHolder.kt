package com.dlgdev.doghandling.ui.dogs

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import java.util.function.IntConsumer

class DogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @BindView (R.id.dog_item_name) lateinit var dogName: TextView
    @BindView(R.id.dog_location) lateinit var dogAdditionalInfo: TextView
    @BindView(R.id.delete_dog) lateinit var delete: ImageView
    @BindView(R.id.edit_dog) lateinit var edit: ImageView

    fun dog(dog: Dog, location: Location, navigate: IntConsumer) {
        itemView.setOnClickListener {
            navigate.accept(R.id.action_dogListFragment_to_viewDogFragment)
        }
        dogName.text = dog.dogName
        dogAdditionalInfo.text = location.locationName
    }
}
