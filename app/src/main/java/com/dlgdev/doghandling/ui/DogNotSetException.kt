package com.dlgdev.doghandling.ui

class DogNotSetException(s: String) : RuntimeException(s)