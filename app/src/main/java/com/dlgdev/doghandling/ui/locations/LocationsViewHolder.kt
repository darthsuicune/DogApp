package com.dlgdev.doghandling.ui.locations

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.Location
import java.util.function.IntConsumer

class LocationsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @BindView(R.id.location_name) lateinit var locationName: TextView
    @BindView(R.id.location_address) lateinit var locationAddress: TextView

    fun location(location: Location, navigate: IntConsumer) {
        itemView.setOnClickListener {
            navigate.accept(R.id.action_locationsListFragment_to_viewLocationFragment)
        }
        locationName.text = location.locationName
        locationAddress.text = location.locationAddress
    }
}
