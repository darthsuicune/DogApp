package com.dlgdev.doghandling.ui.dogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import com.dlgdev.doghandling.ui.dogs.viewmodel.ViewDogViewModel
import kotlinx.android.synthetic.main.view_dog_fragment.*
import javax.inject.Inject

class ViewDogFragment : DaggerFragmentWithMenu() {

    companion object {
        val ARG_DOG_ID = "dog_id"
        fun newInstance() = ViewDogFragment()
    }

    @Inject lateinit var factory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_dog_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel: ViewDogViewModel by viewModels { factory }
        viewModel.dogId = arguments!![ARG_DOG_ID] as Long

        viewModel.dog.observe(viewLifecycleOwner, Observer { dog ->
            dog_name.text = dog.dogName
            dog_contact.text = dog.dogContactPhone
            dog_notes.text = dog.dogNotes
            dog_owner.text = dog.ownersName

            viewModel.location.observe(viewLifecycleOwner, Observer { loc ->
                location_name.text = loc.locationName
                location_address.text = loc.locationAddress
                location_contact.text = loc.locationContactPhone
                location_notes.text = loc.locationNotes
            })
        })
    }

}
