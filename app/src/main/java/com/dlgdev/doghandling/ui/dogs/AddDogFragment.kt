package com.dlgdev.doghandling.ui.dogs

import android.os.Bundle
import android.view.*
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import com.dlgdev.doghandling.ui.LocationNotSetException
import com.dlgdev.doghandling.ui.LocationPickerFragment
import com.dlgdev.doghandling.ui.appointments.AddAppointmentFragment
import com.dlgdev.doghandling.ui.dogs.viewmodel.AddDogViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.add_appointment_fragment.*
import kotlinx.android.synthetic.main.add_dog_fragment.*
import javax.inject.Inject

class AddDogFragment : DaggerFragmentWithMenu() {

    companion object {
        fun newInstance() = AddDogFragment()
        const val LOCATION_PICKER_TAG = "locationPicker"
    }

    @Inject lateinit var factory: ViewModelProvider.Factory
    lateinit var viewModel: AddDogViewModel

    private val onLocationPickedListener =
            object : LocationPickerFragment.OnLocationPickedListener {
                override fun onLocationPicked(location: Location) {
                    viewModel.location = location
                    add_appointment_location_name.text = location.locationName
                }
            }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_dog_fragment, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.save_action, menu)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(AddDogViewModel::class.java)

        add_dog_location_block.setOnClickListener {
            findLocation()
        }
        (childFragmentManager.findFragmentByTag(LOCATION_PICKER_TAG) as LocationPickerFragment?)
                ?.addListener(onLocationPickedListener)
    }

    private fun findLocation() {
        val fr = LocationPickerFragment()
        fr.addListener(onLocationPickedListener)
        fr.show(childFragmentManager, LOCATION_PICKER_TAG)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                attemptCreateDog()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun attemptCreateDog() {
        try {
            val name = add_dog_dog_name.text.toString()
            val owner = dog_details_owner_name.text.toString()
            val phone = dog_details_owner_phone.text.toString()
            val notes = dog_details_notes.text.toString()
            viewModel.createDog(name, owner, phone, notes)
            findNavController().popBackStack()
        } catch (e: LocationNotSetException) {
            Snackbar.make(add_dog_fragment, R.string.location_not_set,
                    Snackbar.LENGTH_SHORT).show()
        }
    }

}
