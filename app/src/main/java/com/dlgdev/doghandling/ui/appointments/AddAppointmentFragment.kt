package com.dlgdev.doghandling.ui.appointments

import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.text.format.DateUtils
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.ui.*
import com.dlgdev.doghandling.ui.appointments.viewmodel.AddAppointmentViewModel
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.add_appointment_fragment.*
import javax.inject.Inject

class AddAppointmentFragment : DaggerFragmentWithMenu() {

    companion object {
        fun newInstance() = AddAppointmentFragment()
        const val DATE_PICKER_TAG = "datePicker"
        const val TIME_PICKER_TAG = "timePicker"
        const val DOG_PICKER_TAG = "dogPicker"
        const val LOCATION_PICKER_TAG = "locationPicker"
    }

    @Inject lateinit var factory: ViewModelProvider.Factory
    lateinit var viewModel: AddAppointmentViewModel

    private val onDateChangedListener =
            MaterialPickerOnPositiveButtonClickListener<Long> {
                viewModel.setNewStartDate(it)
                displayDateAndTime()
            }
    private val onTimeChangedListener =
            MaterialPickerOnPositiveButtonClickListener<Long> {
                viewModel.setNewStartTime(it)
                displayDateAndTime()
            }
    private val onDogPickedListener = object : DogPickerFragment.OnDogPickedListener {
        override fun onDogPicked(dog: Dog) {
            viewModel.dog = dog
            add_appointment_dog_name.text = dog.toString()
        }
    }
    private val onLocationPickedListener =
            object : LocationPickerFragment.OnLocationPickedListener {
                override fun onLocationPicked(location: Location) {
                    viewModel.location = location
                    add_appointment_location_name.text = location.locationName
                }
            }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_appointment_fragment, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.save_action, menu)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity!!.title = getString(R.string.new_appointment_title)
        viewModel = ViewModelProvider(this, factory).get(
                AddAppointmentViewModel::class.java)
        displayDateAndTime()

        setOnClickListeners()

        reattachFragmentListeners()
    }

    // Suppressing unchecked cast due to the <Long>. Can't extend.
    @Suppress("UNCHECKED_CAST")
    private fun reattachFragmentListeners() {
        // On rotation, fragments lose their listeners because they are recreated. This code
        // ensures they are added back before they can be interacted with.
        (childFragmentManager.findFragmentByTag(DATE_PICKER_TAG) as MaterialDatePicker<Long>?)
                ?.addOnPositiveButtonClickListener(onDateChangedListener)
        (childFragmentManager.findFragmentByTag(TIME_PICKER_TAG) as TimePickerFragment?)
                ?.addOnTimeSetListener(onTimeChangedListener)
        (childFragmentManager.findFragmentByTag(DOG_PICKER_TAG) as DogPickerFragment?)
                ?.addListener(onDogPickedListener)
        (childFragmentManager.findFragmentByTag(LOCATION_PICKER_TAG) as LocationPickerFragment?)
                ?.addListener(onLocationPickedListener)
    }

    private fun setOnClickListeners() {
        appointment_start_date.setOnClickListener { changeDate() }
        appointment_start_time.setOnClickListener { changeTime() }
        add_appointment_dog_block.setOnClickListener { findDogForAppointment() }
        add_appointment_location_block.setOnClickListener { findLocationForAppointment() }
        add_appointment_new_dog.setOnClickListener { requestNewDog() }
        add_appointment_new_dog.setOnClickListener { requestNewLocation() }
    }

    private fun displayDateAndTime() {
        appointment_start_date.text = DateUtils.formatDateTime(context, viewModel.startTime,
                DateUtils.FORMAT_ABBREV_MONTH or
                        DateUtils.FORMAT_SHOW_DATE or
                        DateUtils.FORMAT_SHOW_YEAR)
        appointment_start_time.text = DateUtils.formatDateTime(context, viewModel.startTime,
                DateUtils.FORMAT_SHOW_TIME)
    }

    private fun requestNewLocation() {
        TODO("Implement moving to the fragment to create a new location")
    }

    private fun requestNewDog() {
        TODO("Implement moving to the fragment to create a new dog")
    }

    private fun changeTime() {
        val picker = TimePickerFragment.Builder()
                .setTime(viewModel.startTime)
                .build()
        picker.addOnTimeSetListener(onTimeChangedListener)
        picker.show(childFragmentManager, TIME_PICKER_TAG)
    }

    private fun changeDate() {
        val picker = MaterialDatePicker.Builder.datePicker()
                .setSelection(viewModel.startTime)
                .build()
        picker.addOnPositiveButtonClickListener(onDateChangedListener)
        picker.show(childFragmentManager, DATE_PICKER_TAG)
    }

    private fun findLocationForAppointment() {
        val fr = LocationPickerFragment()
        fr.addListener(onLocationPickedListener)
        fr.show(childFragmentManager, LOCATION_PICKER_TAG)
    }

    private fun findDogForAppointment() {
        val fr = DogPickerFragment()
        fr.addListener(onDogPickedListener)
        fr.show(childFragmentManager, DOG_PICKER_TAG)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                attemptCreatingAppointment()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun attemptCreatingAppointment() {
        val treatment = appointment_treatment.text.toString()
        val notes = appointment_notes.text.toString()
        val cost = appointment_cost.text.toString()
        try {
            //TODO: Replace hiding the keyboard and snackbars with proper text fields.
            (context!!.getSystemService(
                    INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                    add_appointment_fragment.windowToken, 0)
            viewModel.createAppointment(treatment, notes, cost)
            findNavController().popBackStack()
        } catch (e: DogNotSetException) {
            Snackbar.make(add_appointment_fragment, R.string.dog_not_set,
                    Snackbar.LENGTH_SHORT).show()
        } catch (e: LocationNotSetException) {
            Snackbar.make(add_appointment_fragment, R.string.location_not_set,
                    Snackbar.LENGTH_SHORT).show()
        }
    }
}
