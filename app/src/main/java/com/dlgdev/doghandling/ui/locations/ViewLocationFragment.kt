package com.dlgdev.doghandling.ui.locations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import kotlinx.android.synthetic.main.view_location_fragment.*
import javax.inject.Inject

class ViewLocationFragment : DaggerFragmentWithMenu() {

    companion object {
        val ARG_LOCATION_ID = "locationId"
        fun newInstance() = ViewLocationFragment()
    }

    @Inject lateinit var factory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_location_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel: ViewLocationViewModel by viewModels { factory }

        viewModel.locationId = arguments!![ARG_LOCATION_ID] as Long

        viewModel.location.observe(viewLifecycleOwner, Observer { loc ->
            location_name.text = loc.locationName
            location_address.text = loc.locationAddress
            location_contact.text = loc.locationContactPhone
            location_notes.text = loc.locationNotes
        })

    }

}
