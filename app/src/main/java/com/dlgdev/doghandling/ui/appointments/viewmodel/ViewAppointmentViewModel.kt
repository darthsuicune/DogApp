package com.dlgdev.doghandling.ui.appointments.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dlgdev.doghandling.model.AppointmentWithDogAtLocation
import com.dlgdev.doghandling.model.data.AppointmentsProvider
import javax.inject.Inject

class ViewAppointmentViewModel @Inject constructor(val appointmentsProvider: AppointmentsProvider)
    : ViewModel() {

    var appointmentId: Long = 0

    val appointment: LiveData<AppointmentWithDogAtLocation> by lazy {
        appointmentsProvider.findAppointmentById(appointmentId)
    }
}
