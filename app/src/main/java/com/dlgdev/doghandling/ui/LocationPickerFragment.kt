package com.dlgdev.doghandling.ui

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListAdapter
import androidx.lifecycle.Observer
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.LocationsProvider
import javax.inject.Inject

class LocationPickerFragment: InjectableDialogFragment() {

    @Inject lateinit var locationProvider: LocationsProvider
    var locations: List<Location> = emptyList()
    lateinit var listener: OnLocationPickedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val adapter = ArrayAdapter(context!!, android.R.layout.simple_list_item_1, mutableListOf(""))

        locationProvider.getLocations().observe(this, Observer {
            locations = it
            adapter.clear()
            adapter.addAll(it.map { location -> location.locationName })
        })

        return AlertDialog.Builder(context)
                .setTitle(R.string.location_picker_title)
                .setAdapter(adapter) { _, position ->
                    listener.onLocationPicked(locations[position])
                }
                .create()
    }

    fun addListener(onLocationPickedListener: OnLocationPickedListener) {
        this.listener = onLocationPickedListener
    }

    interface OnLocationPickedListener {
        fun onLocationPicked(location: Location)
    }
}