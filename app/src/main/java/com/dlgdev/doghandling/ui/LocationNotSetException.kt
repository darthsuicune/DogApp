package com.dlgdev.doghandling.ui

class LocationNotSetException(s: String) : RuntimeException(s)
