package com.dlgdev.doghandling.ui.appointments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import com.dlgdev.doghandling.ui.appointments.viewmodel.ViewAppointmentViewModel
import kotlinx.android.synthetic.main.view_appointment_fragment.*
import javax.inject.Inject

class ViewAppointmentFragment : DaggerFragmentWithMenu() {

    companion object {
        fun newInstance() = ViewAppointmentFragment()
        const val ARG_APPOINTMENT_ID = "appointmentId"
    }

    @Inject lateinit var factory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_appointment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        finish_appointment.setOnClickListener {
            findNavController().navigate(
                    R.id.action_viewAppointmentFragment_to_editAppointmentFragment, Bundle().also {
                it.putLong(EditAppointmentFragment.ARG_APPOINTMENT_ID,
                        arguments!![ARG_APPOINTMENT_ID] as Long)
                it.putBoolean(EditAppointmentFragment.ARG_APPOINTMENT_CANCEL, false)
            })
        }
        cancel_appointment.setOnClickListener {
            findNavController().navigate(
                    R.id.action_viewAppointmentFragment_to_editAppointmentFragment, Bundle().also {
                it.putLong(EditAppointmentFragment.ARG_APPOINTMENT_ID,
                        arguments!![ARG_APPOINTMENT_ID] as Long)
                it.putBoolean(EditAppointmentFragment.ARG_APPOINTMENT_CANCEL, true)
            })
        }
        modify_appointment.setOnClickListener {
            findNavController().navigate(
                    R.id.action_viewAppointmentFragment_to_editAppointmentFragment, Bundle().also {
                it.putLong(EditAppointmentFragment.ARG_APPOINTMENT_ID,
                        arguments!![ARG_APPOINTMENT_ID] as Long)
            })
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel: ViewAppointmentViewModel by viewModels { factory }

        viewModel.appointmentId = arguments!![ARG_APPOINTMENT_ID] as Long

        viewModel.appointment.observe(viewLifecycleOwner, Observer {
            val appointment = it.appointment
            val dog = it.dog
            val location = it.location

            appointment_start_date.text = appointment.displayStartDate()
            appointment_behavior.text = appointment.behavior
            appointment_cost.text = appointment.cost
            appointment_notes.text = appointment.notes
            appointment_treatment.text = appointment.treatment
            appointment_end_time.text = appointment.displayEndDate()

            appointment_dog_contact.text = dog.dogContactPhone
            appointment_dog_name.text = dog.dogName
            appointment_dog_notes.text = dog.dogNotes
            appointment_dog_owner.text = dog.ownersName

            appointment_location_address.text = location.locationAddress
            appointment_location_contact.text = location.locationContactPhone
            appointment_location_name.text = location.locationName
            appointment_location_notes.text = location.locationNotes
        })
    }

}
