package com.dlgdev.doghandling.ui.appointments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.AppointmentWithDogAtLocation
import java.util.function.IntConsumer
import javax.inject.Inject

class AppointmentsRecyclerAdapter @Inject constructor() :
        RecyclerView.Adapter<AppointmentsViewHolder>() {
    var appointments: List<AppointmentWithDogAtLocation> = emptyList()
    var navController: NavController? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentsViewHolder =
            AppointmentsViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.appointments_item_view,
                            parent, false))

    override fun getItemCount(): Int = appointments.size

    override fun onBindViewHolder(holder: AppointmentsViewHolder, position: Int) {
        ButterKnife.bind(holder, holder.itemView)
        val appointment = appointments[position]

        holder.appointment(appointment.appointment, appointment.dog, appointment.location,
                IntConsumer { action ->
                    navController?.navigate(action, Bundle().also {
                        it.putLong(ViewAppointmentFragment.ARG_APPOINTMENT_ID, appointment.appointmentId())
                    })
                })
    }

    fun updateAppointments(appointments: List<AppointmentWithDogAtLocation>) {
        this.appointments = appointments
        this.notifyDataSetChanged()
    }
}