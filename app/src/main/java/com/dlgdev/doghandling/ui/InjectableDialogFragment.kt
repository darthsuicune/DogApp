package com.dlgdev.doghandling.ui

import android.content.Context
import androidx.fragment.app.DialogFragment
import dagger.android.support.AndroidSupportInjection

open class InjectableDialogFragment: DialogFragment() {
    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}
