package com.dlgdev.doghandling.ui

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.format.DateFormat
import androidx.fragment.app.DialogFragment
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener
import java.util.*

class TimePickerFragment : DialogFragment() {

    private val c = Calendar.getInstance()
    private val listeners: LinkedHashSet<MaterialPickerOnPositiveButtonClickListener<Long>> =
            LinkedHashSet(1)
    private val onTimeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
        c[Calendar.HOUR_OF_DAY] = hour
        c[Calendar.MINUTE] = minute
        for (listener in listeners) {
            listener.onPositiveButtonClick(c.timeInMillis)
        }
    }

    companion object {
        const val ARG_TIME = "time"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = savedInstanceState ?: arguments
        if (bundle != null) {
            c.timeInMillis = bundle[ARG_TIME] as Long
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(ARG_TIME, c.timeInMillis)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return TimePickerDialog(activity, onTimeSetListener, c[Calendar.HOUR_OF_DAY],
                c[Calendar.MINUTE], DateFormat.is24HourFormat(activity))
    }

    fun addOnTimeSetListener(listener: MaterialPickerOnPositiveButtonClickListener<Long>) {
        this.listeners.add(listener)
    }

    class Builder {
        var builderTime = 0L

        fun setTime(newTime: Long): Builder {
            builderTime = newTime
            return this
        }

        fun build(): TimePickerFragment {
            if (0L == builderTime) {
                throw IllegalStateException("Time is not set")
            }
            val fr = TimePickerFragment()
            val args = Bundle().also {
                it.putLong(ARG_TIME, builderTime)
            }
            fr.arguments = args
            return fr
        }
    }
}