package com.dlgdev.doghandling.ui

import android.content.Context
import androidx.navigation.fragment.findNavController
import dagger.android.support.DaggerFragment

abstract class DaggerFragmentWithMenu : DaggerFragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setHasOptionsMenu(true)
    }

    override fun onDetach() {
        super.onDetach()
        setHasOptionsMenu(false)
    }

    fun navigateTo(action: Int) {
        findNavController().navigate(action)
    }
}