package com.dlgdev.doghandling.ui.dogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.usecases.EditDogUseCase
import com.dlgdev.doghandling.ui.DaggerFragmentWithMenu
import com.dlgdev.doghandling.ui.dogs.viewmodel.EditDogViewModel
import javax.inject.Inject

class EditDogFragment : DaggerFragmentWithMenu() {

    companion object {
        fun newInstance() = EditDogFragment()
    }

    @Inject lateinit var factory: ViewModelProvider.Factory

    @Inject lateinit var editDog: EditDogUseCase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.edit_dog_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel: EditDogViewModel by viewModels { factory }
        // TODO: Use the ViewModel
    }

}
