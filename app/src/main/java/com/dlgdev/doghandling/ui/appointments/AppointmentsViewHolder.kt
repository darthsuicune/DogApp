package com.dlgdev.doghandling.ui.appointments

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import java.util.function.IntConsumer

class AppointmentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @BindView(R.id.appointment_dog_name) lateinit var dogName: TextView
    @BindView(R.id.appointment_location_name) lateinit var locationName: TextView
    @BindView(R.id.appointment_start_date) lateinit var startTime: TextView

    fun appointment(appointment: Appointment, dog: Dog, location: Location,
                    navigate: IntConsumer) {
        itemView.setOnClickListener {
            navigate.accept(R.id.action_appointmentListFragment_to_viewAppointmentFragment)
        }
        dogName.text = dog.dogName
        locationName.text = location.locationName
        startTime.text = appointment.displayStartDate()
    }

}
