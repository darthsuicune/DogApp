package com.dlgdev.doghandling.ui.appointments.viewmodel

import androidx.lifecycle.ViewModel
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.usecases.CreateAppointmentUseCase
import com.dlgdev.doghandling.ui.DogNotSetException
import com.dlgdev.doghandling.ui.LocationNotSetException
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.thread

class AddAppointmentViewModel @Inject constructor(val useCase: CreateAppointmentUseCase) :
        ViewModel() {

    var dog: Dog? = null
    var location: Location? = null
    var startTime: Long = Date().time

    fun createAppointment(treatment: String, notes: String, cost: String) {
        if (dog == null) {
            throw DogNotSetException("dog is still not set")
        }
        if (location == null) {
            throw LocationNotSetException("location is still not set")
        }

        thread {
            useCase.create(dog!!, location!!, startTime, treatment, notes, cost)
        }

    }

    fun setNewStartTime(newTime: Long) {
        val newCal = Calendar.getInstance().also {it.timeInMillis = newTime}
        val previousCal = Calendar.getInstance().also { it.timeInMillis = startTime }
        previousCal[Calendar.HOUR_OF_DAY] = newCal[Calendar.HOUR_OF_DAY]
        previousCal[Calendar.MINUTE] = newCal[Calendar.MINUTE]
        startTime = previousCal.timeInMillis
    }

    fun setNewStartDate(newDate: Long) {
        val newCal = Calendar.getInstance().also { it.timeInMillis = newDate}
        val previousCal = Calendar.getInstance().also { it.timeInMillis = startTime }
        previousCal[Calendar.YEAR] = newCal[Calendar.YEAR]
        previousCal[Calendar.MONTH] = newCal[Calendar.MONTH]
        previousCal[Calendar.DAY_OF_MONTH] = newCal[Calendar.DAY_OF_MONTH]
        startTime = previousCal.timeInMillis

    }
}
