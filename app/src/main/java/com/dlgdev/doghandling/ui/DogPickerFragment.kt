package com.dlgdev.doghandling.ui

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.data.DogsProvider
import javax.inject.Inject

class DogPickerFragment : InjectableDialogFragment() {

    @Inject lateinit var dogsProvider: DogsProvider
    var dogs: List<Dog> = emptyList()
    lateinit var listener: OnDogPickedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val adapter = ArrayAdapter(context!!, android.R.layout.simple_list_item_1, mutableListOf(""))
        dogsProvider.getDogs().observe(this, Observer {
            dogs = it
            adapter.clear()
            adapter.addAll(it.map { dog -> dog.toString() })
        })
        return AlertDialog.Builder(context)
                .setTitle(R.string.dog_picker_title)
                .setAdapter(adapter) { _, position ->
                    listener.onDogPicked(dogs[position])
                }
                .create()
    }

    fun addListener(listener: OnDogPickedListener) {
        this.listener = listener
    }

    interface OnDogPickedListener {
        fun onDogPicked(dog: Dog)
    }
}