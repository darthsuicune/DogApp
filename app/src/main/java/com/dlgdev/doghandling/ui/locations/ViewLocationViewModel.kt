package com.dlgdev.doghandling.ui.locations

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.LocationsProvider
import javax.inject.Inject

class ViewLocationViewModel @Inject constructor(val provider: LocationsProvider): ViewModel() {

    var locationId: Long = 0

    val location: LiveData<Location> by lazy {
        provider.getLocationForId(locationId)
    }
}
