package com.dlgdev.doghandling.ui.dogs.viewmodel

import androidx.lifecycle.ViewModel
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.usecases.CreateDogUseCase
import com.dlgdev.doghandling.ui.LocationNotSetException
import javax.inject.Inject
import kotlin.concurrent.thread

class AddDogViewModel @Inject constructor(val useCase: CreateDogUseCase) : ViewModel() {

    var location: Location? = null

    fun createDog(name: String, ownersName: String, contactPhone: String, comments: String) {
        if (location == null) {
            throw LocationNotSetException("Location not set when creating dog")
        }

        thread {
            useCase.createNewDog(name, ownersName, contactPhone, comments, location!!)
        }
    }
}
