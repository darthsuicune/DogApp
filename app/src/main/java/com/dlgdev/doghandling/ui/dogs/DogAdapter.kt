package com.dlgdev.doghandling.ui.dogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.model.DogWithLocation
import com.dlgdev.doghandling.model.Location
import java.util.function.IntConsumer
import javax.inject.Inject

class DogAdapter @Inject constructor() : RecyclerView.Adapter<DogViewHolder>() {
    var navController: NavController? = null
    var dogs: List<DogWithLocation> = emptyList()
    var locations: List<Location> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, position: Int) =
            DogViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.dog_item, parent,
                            false))

    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {
        ButterKnife.bind(holder, holder.itemView)
        val dog = dogs[position]

        holder.dog(dog.dog, dog.location,
                IntConsumer { action ->
                    navController?.navigate(action, Bundle().also {
                        it.putLong(ViewDogFragment.ARG_DOG_ID, dog.dogId())
                    })
                })
    }

    override fun getItemCount() = dogs.size

    fun updateDogs(dogs: List<DogWithLocation>) {
        this.dogs = dogs
        this.notifyDataSetChanged()
    }
}

