package com.dlgdev.doghandling.ui.appointments.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dlgdev.doghandling.model.AppointmentWithDogAtLocation
import com.dlgdev.doghandling.model.data.AppointmentsProvider
import javax.inject.Inject

class AppointmentListViewModel @Inject constructor(val provider: AppointmentsProvider) :
        ViewModel() {

    val appointments: LiveData<List<AppointmentWithDogAtLocation>> by lazy {
        provider.getAppointmentsWithDogsAndLocations()
    }
}
