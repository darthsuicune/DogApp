package com.dlgdev.doghandling.ui.dogs.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.DogsProvider
import com.dlgdev.doghandling.model.data.LocationsProvider
import javax.inject.Inject

class ViewDogViewModel @Inject constructor(val locationsProvider: LocationsProvider,
                                           val dogsProvider: DogsProvider) : ViewModel() {

    var dogId: Long = 0

    val dog: LiveData<Dog> by lazy {
        dogsProvider.getDogForId(dogId)
    }
    val location: LiveData<Location> by lazy {
        locationsProvider.getLocationForId(dog.value!!.dogDefaultLocationId)
    }
}
