package com.dlgdev.doghandling.ui.dogs.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dlgdev.doghandling.model.DogWithLocation
import com.dlgdev.doghandling.model.data.DogsProvider
import javax.inject.Inject

class DogListViewModel @Inject constructor(val dogsProvider: DogsProvider) : ViewModel() {

    val dogs: LiveData<List<DogWithLocation>> by lazy {
        dogsProvider.getDogsWithLocation()
    }
}
