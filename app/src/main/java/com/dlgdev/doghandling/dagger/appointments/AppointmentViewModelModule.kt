package com.dlgdev.doghandling.dagger.appointments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dlgdev.doghandling.dagger.ViewModelFactory
import com.dlgdev.doghandling.dagger.ViewModelKey
import com.dlgdev.doghandling.ui.appointments.viewmodel.AddAppointmentViewModel
import com.dlgdev.doghandling.ui.appointments.viewmodel.AppointmentListViewModel
import com.dlgdev.doghandling.ui.appointments.viewmodel.EditAppointmentViewModel
import com.dlgdev.doghandling.ui.appointments.viewmodel.ViewAppointmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AppointmentViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(
            AppointmentListViewModel::class)
    abstract fun bindAppointmentListViewModel(userViewModel: AppointmentListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(
            AddAppointmentViewModel::class)
    abstract fun bindAddAppointmentViewModel(searchViewModel: AddAppointmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(
            EditAppointmentViewModel::class)
    abstract fun bindEditAppointmentViewModel(repoViewModel: EditAppointmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(
            ViewAppointmentViewModel::class)
    abstract fun bindViewAppointmentViewModel(repoViewModel: ViewAppointmentViewModel): ViewModel

    @Binds
    abstract fun provideViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}