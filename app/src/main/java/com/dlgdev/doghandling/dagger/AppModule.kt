package com.dlgdev.doghandling.dagger

import com.dlgdev.doghandling.dagger.appointments.AppointmentViewModelModule
import com.dlgdev.doghandling.dagger.dogs.DogViewModelModule
import com.dlgdev.doghandling.dagger.locations.LocationViewModelModule
import dagger.Module

@Module(includes = [AppointmentViewModelModule::class, DogViewModelModule::class,
    LocationViewModelModule::class])
abstract class AppModule
