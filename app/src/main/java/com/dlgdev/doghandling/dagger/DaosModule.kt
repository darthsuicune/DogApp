package com.dlgdev.doghandling.dagger

import com.dlgdev.doghandling.model.data.AppointmentDao
import com.dlgdev.doghandling.model.data.DogDao
import com.dlgdev.doghandling.model.data.DogDatabase
import com.dlgdev.doghandling.model.data.LocationDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DaosModule {
    @Singleton
    @Provides
    fun provideDogDao(db: DogDatabase): DogDao {
        return db.dogDao()
    }

    @Singleton
    @Provides
    fun provideAppointmentDao(db: DogDatabase): AppointmentDao {
        return db.appointmentDao()
    }

    @Singleton
    @Provides
    fun provideLocationDao(db: DogDatabase): LocationDao {
        return db.locationDao()
    }
}