package com.dlgdev.doghandling.dagger.locations

import com.dlgdev.doghandling.ui.LocationPickerFragment
import com.dlgdev.doghandling.ui.locations.AddLocationFragment
import com.dlgdev.doghandling.ui.locations.EditLocationFragment
import com.dlgdev.doghandling.ui.locations.LocationListFragment
import com.dlgdev.doghandling.ui.locations.ViewLocationFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LocationFragmentsModule {
    @ContributesAndroidInjector
    abstract fun provideAddLocationFragment() : AddLocationFragment
    @ContributesAndroidInjector
    abstract fun provideEditLocationFragment() : EditLocationFragment
    @ContributesAndroidInjector
    abstract fun provideLocationListFragment(): LocationListFragment
    @ContributesAndroidInjector
    abstract fun provideViewLocationFragment() : ViewLocationFragment
    @ContributesAndroidInjector
    abstract fun provideLocationPickerFragment() : LocationPickerFragment
}