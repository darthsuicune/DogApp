package com.dlgdev.doghandling.dagger.dogs

import androidx.lifecycle.ViewModel
import com.dlgdev.doghandling.dagger.ViewModelKey
import com.dlgdev.doghandling.ui.dogs.viewmodel.AddDogViewModel
import com.dlgdev.doghandling.ui.dogs.viewmodel.DogListViewModel
import com.dlgdev.doghandling.ui.dogs.viewmodel.EditDogViewModel
import com.dlgdev.doghandling.ui.dogs.viewmodel.ViewDogViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DogViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(DogListViewModel::class)
    abstract fun bindDogListViewModel(userViewModel: DogListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddDogViewModel::class)
    abstract fun bindAddDogViewModel(searchViewModel: AddDogViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditDogViewModel::class)
    abstract fun bindEditDogViewModel(repoViewModel: EditDogViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ViewDogViewModel::class)
    abstract fun bindViewDogViewModel(repoViewModel: ViewDogViewModel): ViewModel
}