package com.dlgdev.doghandling.dagger

import com.dlgdev.doghandling.model.data.AppointmentsProvider
import com.dlgdev.doghandling.model.data.DatabaseProvider
import com.dlgdev.doghandling.model.data.DogsProvider
import com.dlgdev.doghandling.model.data.LocationsProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [DaosModule::class])
abstract class ProvidersModule {
    @Singleton @Binds
    abstract fun provideDogsProvider(db: DatabaseProvider) : DogsProvider

    @Singleton @Binds
    abstract fun provideLocationsProvider(db: DatabaseProvider) : LocationsProvider

    @Singleton @Binds
    abstract fun provideAppointmentsProvider(db: DatabaseProvider) : AppointmentsProvider
}