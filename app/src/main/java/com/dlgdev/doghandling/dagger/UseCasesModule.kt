package com.dlgdev.doghandling.dagger

import com.dlgdev.doghandling.model.usecases.*
import com.dlgdev.doghandling.model.usecases.impl.*
import dagger.Binds
import dagger.Module

@Module(includes = [DbModule::class])
abstract class UseCasesModule {
    @Binds abstract fun provideCreateDog(useCase: CreateDog): CreateDogUseCase
    @Binds abstract fun provideCreateAppointment(useCase: CreateAppointment): CreateAppointmentUseCase
    @Binds abstract fun provideCancelAppointment(useCase: CancelAppointment): CancelAppointmentUseCase
    @Binds abstract fun provideEditAppointment(useCase: EditAppointment): EditAppointmentUseCase
    @Binds abstract fun provideEditDog(useCase: EditDog): EditDogUseCase
    @Binds abstract fun provideFinishAppointment(useCase: FinishAppointment): FinishAppointmentUseCase
}