package com.dlgdev.doghandling.dagger

import android.app.Application
import androidx.room.Room
import com.dlgdev.doghandling.model.data.DogDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ProvidersModule::class])
class DbModule {
    @Singleton
    @Provides
    fun provideDogDb(app: Application): DogDatabase {
            return Room.databaseBuilder(app, DogDatabase::class.java, "dogs.db")
                    .fallbackToDestructiveMigration()
                    .build()
    }
}