package com.dlgdev.doghandling.dagger.locations

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dlgdev.doghandling.dagger.ViewModelFactory
import com.dlgdev.doghandling.dagger.ViewModelKey
import com.dlgdev.doghandling.ui.locations.AddLocationViewModel
import com.dlgdev.doghandling.ui.locations.LocationListViewModel
import com.dlgdev.doghandling.ui.locations.EditLocationViewModel
import com.dlgdev.doghandling.ui.locations.ViewLocationViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
abstract class LocationViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(LocationListViewModel::class)
    abstract fun bindLocationListViewModel(userViewModel: LocationListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddLocationViewModel::class)
    abstract fun bindAddLocationViewModel(searchViewModel: AddLocationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditLocationViewModel::class)
    abstract fun bindEditLocationViewModel(repoViewModel: EditLocationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ViewLocationViewModel::class)
    abstract fun bindViewLocationViewModel(repoViewModel: ViewLocationViewModel): ViewModel
}