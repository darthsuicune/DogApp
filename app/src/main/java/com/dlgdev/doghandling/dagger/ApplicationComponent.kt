package com.dlgdev.doghandling.dagger

import android.app.Application
import com.dlgdev.doghandling.App
import com.dlgdev.doghandling.dagger.appointments.AppointmentViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [(AndroidSupportInjectionModule::class),
            (AppModule::class),
            (UseCasesModule::class),
            (MainActivityModule::class)])
interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }
    fun inject(app: App)
}