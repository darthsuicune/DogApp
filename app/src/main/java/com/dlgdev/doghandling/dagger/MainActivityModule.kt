package com.dlgdev.doghandling.dagger

import com.dlgdev.doghandling.dagger.appointments.AppointmentFragmentsModule
import com.dlgdev.doghandling.dagger.dogs.DogFragmentsModule
import com.dlgdev.doghandling.dagger.locations.LocationFragmentsModule
import com.dlgdev.doghandling.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = [(AppointmentFragmentsModule::class),
        (DogFragmentsModule::class),
        (LocationFragmentsModule::class)])
    abstract fun contributeMainActivity(): MainActivity
}