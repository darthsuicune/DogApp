package com.dlgdev.doghandling.dagger.dogs

import com.dlgdev.doghandling.ui.DogPickerFragment
import com.dlgdev.doghandling.ui.dogs.AddDogFragment
import com.dlgdev.doghandling.ui.dogs.DogListFragment
import com.dlgdev.doghandling.ui.dogs.EditDogFragment
import com.dlgdev.doghandling.ui.dogs.ViewDogFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DogFragmentsModule {
    @ContributesAndroidInjector
    abstract fun provideAddDogFragment() : AddDogFragment
    @ContributesAndroidInjector
    abstract fun provideDogListFragment() : DogListFragment
    @ContributesAndroidInjector
    abstract fun provideViewDogFragment() : ViewDogFragment
    @ContributesAndroidInjector
    abstract fun provideEditDogFragment() : EditDogFragment
    @ContributesAndroidInjector
    abstract fun provideDogPickerFragment() : DogPickerFragment
}