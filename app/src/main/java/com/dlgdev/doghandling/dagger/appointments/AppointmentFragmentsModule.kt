package com.dlgdev.doghandling.dagger.appointments

import com.dlgdev.doghandling.ui.appointments.AddAppointmentFragment
import com.dlgdev.doghandling.ui.appointments.AppointmentListFragment
import com.dlgdev.doghandling.ui.appointments.EditAppointmentFragment
import com.dlgdev.doghandling.ui.appointments.ViewAppointmentFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AppointmentFragmentsModule {
    @ContributesAndroidInjector
    abstract fun provideAppointmentListFragment() : AppointmentListFragment
    @ContributesAndroidInjector
    abstract fun provideAddAppointmentFragment() : AddAppointmentFragment
    @ContributesAndroidInjector
    abstract fun provideEditAppointmentFragment() : EditAppointmentFragment
    @ContributesAndroidInjector
    abstract fun provideViewAppointmentFragment() : ViewAppointmentFragment
}