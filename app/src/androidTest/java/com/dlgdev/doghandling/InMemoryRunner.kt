package com.dlgdev.doghandling

import android.app.Application
import android.app.Instrumentation
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

class InMemoryRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?,
                                context: Context?): Application =
            Instrumentation.newApplication(TestApp::class.java, context)
}