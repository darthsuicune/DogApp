package com.dlgdev.doghandling.dagger

import com.dlgdev.doghandling.TestApp
import com.dlgdev.doghandling.ui.appointments.AppointmentsListTest
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class),
    (InMemoryDbModule::class),
    (MainActivityModule::class),
    (UseCasesModule::class)])
interface InMemoryComponent {
    fun inject(app: TestApp)
    fun inject(testSuite: AppointmentsListTest)
}