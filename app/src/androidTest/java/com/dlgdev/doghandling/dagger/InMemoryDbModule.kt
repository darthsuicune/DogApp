package com.dlgdev.doghandling.dagger

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase.CONFLICT_REPLACE
import android.util.Log
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.dlgdev.doghandling.model.Appointment
import com.dlgdev.doghandling.model.Dog
import com.dlgdev.doghandling.model.Location
import com.dlgdev.doghandling.model.data.DogDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ProvidersModule::class])
class InMemoryDbModule(val context: Context) {
    companion object {
        val LOCATION = Location("locationname", "locationaddress",
                "locationphone", "locationnotes")
        val DOG = Dog("dogname", "ownername", "contactphone",
                "dognotes", -1)
        val APPOINTMENT = Appointment(-1, -1, 0L, 0L, "treatment",
                "notes", "cost", "behavior")

    }

    @Singleton
    @Provides
    fun provideDogDb(): DogDatabase {
        return Room.inMemoryDatabaseBuilder(context, DogDatabase::class.java)
                .addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        Log.d("Initial insert", "inserting initial set of values")

                        val locId = db.insert("locations", CONFLICT_REPLACE,
                                ContentValues().also {
                                    it.put("name", LOCATION.locationName)
                                    it.put("address", LOCATION.locationAddress)
                                    it.put("contactPhone", LOCATION.locationContactPhone)
                                    it.put("notes", LOCATION.locationNotes)
                                })
                        val dogId = db.insert("dogs", CONFLICT_REPLACE,
                                ContentValues().also {
                                    it.put("name", DOG.dogName)
                                    it.put("ownersName", DOG.ownersName)
                                    it.put("contactPhone", DOG.dogContactPhone)
                                    it.put("notes", DOG.dogNotes)
                                    it.put("locationId", locId)
                                })
                        db.insert("appointments", CONFLICT_REPLACE,
                                ContentValues().also {
                                    it.put("dogId", dogId)
                                    it.put("locationId", locId)
                                    it.put("timeStart", APPOINTMENT.timeStart)
                                    it.put("timeEnd", APPOINTMENT.timeEnd)
                                    it.put("treatment", APPOINTMENT.treatment)
                                    it.put("notes", APPOINTMENT.notes)
                                    it.put("cost", APPOINTMENT.cost)
                                    it.put("behavior", APPOINTMENT.behavior)
                                    it.put("canceled", false)
                                })
                    }
                }).build()
    }
}