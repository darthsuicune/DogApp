package com.dlgdev.doghandling

import com.dlgdev.doghandling.dagger.DaggerInMemoryComponent
import com.dlgdev.doghandling.dagger.InMemoryDbModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Inject

class TestApp : DaggerApplication() {

    @Inject lateinit var injector : DispatchingAndroidInjector<TestApp>

    override fun onCreate() {
        DaggerInMemoryComponent.builder()
                .inMemoryDbModule(InMemoryDbModule(this)).build().inject(this)
        super.onCreate()
    }
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = injector

}