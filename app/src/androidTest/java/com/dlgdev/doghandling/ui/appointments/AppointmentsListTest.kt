package com.dlgdev.doghandling.ui.appointments

import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.dlgdev.doghandling.R
import com.dlgdev.doghandling.dagger.InMemoryDbModule
import com.dlgdev.doghandling.ui.MainActivity
import org.junit.Rule
import org.junit.Test

class AppointmentsListTest {
    @get:Rule val rule = ActivityScenarioRule(MainActivity::class.java)

    @Test fun testAppointmentsAreDisplayed() {
        rule.scenario.moveToState(Lifecycle.State.RESUMED)
        onView(withId(R.id.appointment_dog_name)).check(
                matches(ViewMatchers.withText(InMemoryDbModule.DOG.dogName)))
    }
}