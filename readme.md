Entry point to the app: Appointment list
Top menu: Dog list, location list
FAB: Create new appointment

Dog list:
top menu: appointment list, location list
FAB: Create new dog

Location list:
top menu: appointment list, dog list
FAB: create location

Add appointment: dog, location, when, what, where, notes, expected price
Expect ability to create dogs/locations on the fly if they dont exist
top menu: confirm???
FAB: confirm???

Add dog: name, owner name, contact phone, notes, location (optional)
expect ability to add dates on the fly
top menu: confirm??
fab: confirm?

add location: name, address, contact phone, notes
top menu: confirm??
fab: ???

view appointment:
display: date-time, when, where, dog, what, price, notes
ability to: edit, finish, cancel/reopen

finish appointment:
finish time(auto+edit), final treatment, final price, behavior, notes, edit other fields

view dog
display: all data + last appointment data
ability to: create new appointment

view location:
display: all location data + statistical data total + current/last month